<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# icap

A Helm chart for deploying ClamAV ICAP interface

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository

```console
helm repo add opendesk-clamav https://gitlab.opencode.de/api/v4/projects/1381/packages/helm/stable
helm install my-release --version 4.0.6 opendesk-clamav/icap
```

### Install via OCI Registry

```console
helm repo add opendesk-clamav oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav
helm install my-release --version 4.0.6 opendesk-clamav/icap
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity Note: podAffinityPreset, podAntiAffinityPreset, and nodeAffinityPreset will be ignored when it's set |
| commonAnnotations | object | `{}` | Additional custom annotations to add to all deployed objects. |
| commonLabels | object | `{}` | Additional custom labels to add to all deployed objects. |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` | Enable container privileged escalation. |
| containerSecurityContext.capabilities | object | `{"drop":["ALL"]}` | Security capabilities for container. |
| containerSecurityContext.enabled | bool | `true` | Enable security context. |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` | Mounts the container's root filesystem as read-only. |
| containerSecurityContext.runAsGroup | int | `101` | Process group id. |
| containerSecurityContext.runAsNonRoot | bool | `true` | Run container as user. |
| containerSecurityContext.runAsUser | int | `100` | Process user id. |
| containerSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` | Disallow custom Seccomp profile by setting it to RuntimeDefault. |
| extraEnvVars | list | `[]` | Array with extra environment variables to add to containers.  extraEnvVars:   - name: FOO     value: "bar"  |
| extraVolumeMounts | list | `[]` | Optionally specify extra list of additional volumeMounts. |
| extraVolumes | list | `[]` | Optionally specify extra list of additional volumes |
| fullnameOverride | string | `""` | Provide a name to substitute for the full names of resources. |
| global.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| global.persistence.tmpClaimName | string | `"clamav-tmp"` | Name of existing shared Persistent Volume Claim. |
| global.registry | string | `"docker.io"` | Container registry address. |
| image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| image.registry | string | `"registry.souvap-univention.de"` | Container registry address. This setting has higher precedence than global.registry. |
| image.repository | string | `"souvap/tooling/images/c-icap"` | Container repository string. |
| image.tag | string | `"0.5.10@sha256:ca4574d66aa035621336329bf08b8de9f65dc0f6744e3950ca6a0c5ba8cbd8ba"` | Define image tag. |
| imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| lifecycleHooks | object | `{}` | Lifecycle to automate configuration before or after startup |
| livenessProbe.enabled | bool | `true` | Enables kubernetes LivenessProbe. |
| livenessProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| livenessProbe.initialDelaySeconds | int | `15` | Delay after container start until LivenessProbe is executed. |
| livenessProbe.periodSeconds | int | `20` | Time between probe executions. |
| livenessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| livenessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| nameOverride | string | `""` | String to partially override release name. |
| nodeSelector | object | `{}` | Node labels for pod assignment Ref: https://kubernetes.io/docs/user-guide/node-selection/ |
| podAnnotations | object | `{}` | Pod Annotations. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/ |
| podLabels | object | `{}` | Pod Labels. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/ |
| podSecurityContext.enabled | bool | `true` | Enable security context. |
| podSecurityContext.fsGroup | int | `101` | If specified, all processes of the container are also part of the supplementary group |
| podSecurityContext.fsGroupChangePolicy | string | `"Always"` | Change ownership and permission of the volume before being exposed inside a Pod. |
| readinessProbe.enabled | bool | `true` | Enables kubernetes ReadinessProbe. |
| readinessProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| readinessProbe.initialDelaySeconds | int | `15` | Delay after container start until ReadinessProbe is executed. |
| readinessProbe.periodSeconds | int | `20` | Time between probe executions. |
| readinessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| readinessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| replicaCount | int | `1` | Set the amount of replicas of deployment. |
| resources.limits.cpu | int | `1` | The max amount of CPUs to consume. |
| resources.limits.memory | string | `"128Mi"` | The max amount of RAM to consume. |
| resources.requests.cpu | string | `"10m"` | The amount of CPUs which has to be available on the scheduled node. |
| resources.requests.memory | string | `"16Mi"` | The amount of RAM which has to be available on the scheduled node. |
| service.annotations | object | `{}` | Additional custom annotations |
| service.enabled | bool | `true` | Enable kubernetes service creation. |
| service.ports.icap.containerPort | int | `1344` | Internal port for ClamAV icap. |
| service.ports.icap.port | int | `1344` | Accessible port for ClamAV icap. |
| service.ports.icap.protocol | string | `"TCP"` | Milter service protocol. |
| service.type | string | `"ClusterIP"` | Choose the kind of Service, one of "ClusterIP", "NodePort" or "LoadBalancer". |
| serviceAccount.annotations | object | `{}` | Additional custom annotations for the ServiceAccount. |
| serviceAccount.automountServiceAccountToken | bool | `false` | Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this serviceAccount do not need to use K8s API. |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for pod. |
| serviceAccount.labels | object | `{}` | Additional custom labels for the ServiceAccount. |
| settings.additionalConfiguration | object | `{}` | Additional custom configuration: |
| settings.clamdModClamdHost | string | `"clamav-clamd"` | The (kubernetes) service address of clamd tcp socket. |
| settings.clamdModClamdPort | int | `3310` | The clamd tcp socket port. |
| settings.debugLevel | int | `1` | The level of debugging information to be logged. The acceptable range of levels is between 0 and 10. |
| settings.keepAliveTimeout | int | `600` | The maximum time in seconds waiting for a new requests before a connection will be closed. |
| settings.maxKeepAliveRequests | int | `100` | The maximum number of requests can be served by one connection. |
| settings.maxMemObject | int | `131072` | The maximum memory size in bytes taken by an object which is processed by c-icap . If the size of an object's body is larger than the maximum size a temporary file is used. |
| settings.maxRequestsPerChild | int | `0` | The maximum number of requests that a child process can serve. After this number has been reached, the process dies. The goal of this parameter is to minimize the risk of memory leaks and increase the stability of c-icap. It can be disabled by setting its value to 0. |
| settings.maxServers | int | `4` | The maximum allowed number of server processes. |
| settings.maxSpareThreads | int | `20` | If the number of the available threads is more than number, then the c-icap server kills a child. |
| settings.minSpareThreads | int | `10` | If the number of the available threads is less than number, the c-icap server starts a new child. |
| settings.pipelining | string | `"on"` | Enable or disable ICAP requests pipelining |
| settings.remoteProxyUsers | string | `"off"` | Set it to on if you want to use username provided by the proxy server. This is the recommended way to use users in c-icap. If the RemoteProxyUsers is off and c-icap configured to use users or groups the internal authentication mechanism will be used. |
| settings.serverAdmin | string | `"c-icap-admin"` | The Administrator of this server. Used when displaying information about this server (logs, info service, etc). |
| settings.serverName | string | `"c-icap"` | A name for this server. Used when displaying information about this server (logs, info service, etc). |
| settings.startServers | int | `1` | The initial number of server processes. Each server process generates a number of threads, which serve the requests. |
| settings.supportBuggyClients | string | `"off"` | Try to handle requests from buggy clients, for example ICAP requests missing "\r\n" sequences |
| settings.threadsPerChild | int | `10` | The number of threads per child process. |
| settings.timeout | int | `300` | The time in seconds after which a connection without activity can be canceled. |
| settings.tmpDir | string | `"/var/tmp"` | Optional path to the global temporary directory. |
| settings.virusScanMaxObjectSize | string | `"500M"` | The maximum size of files which will be scanned by antivirus service.You can use K and M indicators to define size in kilobytes or megabytes. |
| settings.virusScanScanFileTypes | string | `"TEXT DATA EXECUTABLE ARCHIVE GIF JPEG MSOFFICE"` | The list of file types or groups of file types which will be scanned for viruses. For supported types look in c-icap.magic configuration file. |
| settings.virusScanSendPercentData | int | `5` | The percentage of data that can be sent by the c-icap server before receiving the complete body of a request. This feature in conjuction with the folowing can be usefull becouse if the download of the object takes a lot of time the connection of web client to proxy can be expired. It must be noticed that the data which delivered to the web client maybe contains a virus or a part of a virus and can be dangerous. In the other hand partial data (for example 5% data of a zip or an exe file) in most cases can not be used. Set it to 0 to disable this feature. |
| settings.virusScanStartSendPercentDataAfter | string | `"2M"` | Only if the object is bigger than size then the percentage of data which defined by SendPercentData sent by the c-icap server before receiving the complete body of request. |
| terminationGracePeriodSeconds | string | `""` | In seconds, time the given to the pod needs to terminate gracefully. Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods |
| tolerations | list | `[]` | Tolerations for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/ |
| topologySpreadConstraints | list | `[]` | Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/  topologySpreadConstraints:   - maxSkew: 1     topologyKey: failure-domain.beta.kubernetes.io/zone     whenUnsatisfiable: DoNotSchedule |
| updateStrategy.type | string | `"RollingUpdate"` | Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pod is destroyed first. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
