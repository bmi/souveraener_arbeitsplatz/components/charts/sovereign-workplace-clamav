{{/*
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
---
apiVersion: {{ include "common.capabilities.deployment.apiVersion" . }}
kind: "Deployment"
metadata:
  name: {{ include "common.names.fullname" . }}
  namespace: {{ include "common.names.namespace" . | quote }}
  labels: {{- include "common.labels.standard" . | nindent 4 }}
    app.kubernetes.io/component: "icap"
    {{- if .Values.commonLabels }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.commonLabels "context" $ ) | nindent 4 }}
    {{- end }}
  {{- if .Values.commonAnnotations }}
  annotations: {{- include "common.tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
  {{- end }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      {{- include "common.labels.matchLabels" . | nindent 6 }}
  strategy: {{ include "common.tplvalues.render" (dict "value" .Values.updateStrategy "context" $) | nindent 4 }}
  template:
    metadata:
      annotations:
        checksum/configmap: {{ include (print .Template.BasePath "/configmap.yaml") . | sha256sum }}
        {{- if .Values.podAnnotations }}
        {{- include "common.tplvalues.render" (dict "value" .Values.podAnnotations "context" $) | nindent 8 }}
        {{- end }}
      labels:
        {{- include "common.labels.standard" . | nindent 8 }}
    spec:
      {{- if or .Values.imagePullSecrets .Values.global.imagePullSecrets }}
      imagePullSecrets:
        {{- range .Values.global.imagePullSecrets }}
        - name: "{{ . }}"
        {{- end }}
        {{- range .Values.imagePullSecrets }}
        - name: "{{ . }}"
        {{- end }}
      {{- end }}
      {{- if .Values.affinity }}
      affinity: {{- include "common.tplvalues.render" (dict "value" .Values.affinity "context" $) | nindent 8 }}
      {{- end }}
      {{- if .Values.tolerations }}
      tolerations: {{- include "common.tplvalues.render" (dict "value" .Values.tolerations "context" $) | nindent 8 }}
      {{- end }}
      {{- if .Values.topologySpreadConstraints }}
        topologySpreadConstraints: {{- include "common.tplvalues.render" (dict "value" .Values.topologySpreadConstraints "context" $) | nindent 8 }}
      {{- end }}
      {{- if .Values.nodeSelector }}
      nodeSelector: {{- include "common.tplvalues.render" (dict "value" .Values.nodeSelector "context" $) | nindent 8 }}
      {{- end }}
      {{- if .Values.podSecurityContext.enabled }}
      securityContext: {{- omit .Values.podSecurityContext "enabled" | toYaml | nindent 8 }}
      {{- end }}
      {{- if .Values.serviceAccount.create }}
      serviceAccountName: {{ include "common.names.fullname" . }}
      {{- end }}
      {{- if .Values.terminationGracePeriodSeconds }}
      terminationGracePeriodSeconds: {{ .Values.terminationGracePeriodSeconds }}
      {{- end }}
      initContainers:
        - name: "config"
          {{- if .Values.containerSecurityContext.enabled }}
          securityContext: {{- omit .Values.containerSecurityContext "enabled" | toYaml | nindent 12 }}
          {{- end }}
          image: "{{ coalesce .Values.image.registry .Values.global.registry }}/{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.imagePullPolicy }}
          command:
            - "/bin/sh"
            - "-c"
            - |
              cp -r /var/lib/clamav/etc/* /config;
              sed -i "s@^clamd_mod.ClamdHost.*@clamd_mod.ClamdHost $(dig +short {{ .Values.settings.clamdModClamdHost }}.{{ .Release.Namespace }}.svc.cluster.local A)@g" /config/clamd_mod.conf;
              while ! nc -z {{ .Values.settings.clamdModClamdHost }} {{ .Values.settings.clamdModClamdPort }}; do
                echo 'Clamd not responding, please check if clamd is running';
                sleep 5;
              done;
          {{- if .Values.resources }}
          resources: {{- include "common.tplvalues.render" (dict "value" .Values.resources "context" $) | nindent 12 }}
          {{- end }}
          volumeMounts:
            - name: "c-icap"
              mountPath: "/var/lib/clamav/etc/c-icap.conf"
              subPath: "c-icap.conf"
            - name: "virus-scan"
              mountPath: "/var/lib/clamav/etc/virus_scan.conf"
              subPath: "virus_scan.conf"
            - name: "clamd-mod"
              mountPath: "/var/lib/clamav/etc/clamd_mod.conf"
              subPath: "clamd_mod.conf"
            - name: "tmpfs-configs"
              mountPath: "/config"
      containers:
        - name: "icap"
          {{- if .Values.containerSecurityContext.enabled }}
          securityContext: {{- omit .Values.containerSecurityContext "enabled" | toYaml | nindent 12 }}
          {{- end }}
          image: "{{ coalesce .Values.image.registry .Values.global.registry }}/{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.imagePullPolicy }}
          command:
            - "/var/lib/clamav/bin/c-icap"
            - "-N"
            - "-D"
          env:
          {{- with .Values.extraEnvVars }}
            {{- . | toYaml | nindent 12 }}
          {{- end }}
          ports:
            {{- range $key, $value := .Values.service.ports }}
            - name: {{ $key }}
              containerPort: {{ $value.containerPort }}
              protocol: {{ $value.protocol }}
            {{- end }}
          livenessProbe:
            tcpSocket:
              port: "icap"
            initialDelaySeconds: {{ .Values.livenessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.livenessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.livenessProbe.timeoutSeconds }}
            failureThreshold: {{ .Values.livenessProbe.failureThreshold }}
            successThreshold: {{ .Values.livenessProbe.successThreshold }}
          readinessProbe:
            tcpSocket:
              port: "icap"
            initialDelaySeconds: {{ .Values.readinessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.readinessProbe.initialDelaySeconds }}
            timeoutSeconds: {{ .Values.readinessProbe.timeoutSeconds }}
            failureThreshold: {{ .Values.readinessProbe.failureThreshold }}
            successThreshold: {{ .Values.readinessProbe.successThreshold }}
          {{- if .Values.resources }}
          resources: {{- include "common.tplvalues.render" (dict "value" .Values.resources "context" $) | nindent 12 }}
          {{- end }}
          {{- if .Values.lifecycleHooks }}
          lifecycle: {{- include "common.tplvalues.render" (dict "value" .Values.lifecycleHooks "context" $) | nindent 12 }}
          {{- end }}
          volumeMounts:
            - name: "tmpfs-configs"
              mountPath: "/var/lib/clamav/etc"
            - name: "tmpfs-var-run"
              mountPath: "/var/run/c-icap"
            - name: "tmpfs-tmp"
              mountPath: "/tmp"
            - name: "clamav-tmp"
              mountPath: "{{ .Values.settings.tmpDir }}"
            {{- if .Values.extraVolumeMounts }}
            {{- include "common.tplvalues.render" (dict "value" .Values.extraVolumeMounts "context" $) | nindent 12 }}
            {{- end }}
      volumes:
        - name: "clamav-tmp"
          persistentVolumeClaim:
            claimName: "{{ .Values.global.persistence.tmpClaimName }}"
        - name: "c-icap"
          configMap:
            name: "{{ include "common.names.fullname" . }}"
            items:
              - key: "c-icap.conf"
                path: "c-icap.conf"
        - name: "virus-scan"
          configMap:
            name: "{{ include "common.names.fullname" . }}"
            items:
              - key: "virus_scan.conf"
                path: "virus_scan.conf"
        - name: "clamd-mod"
          configMap:
            name: "{{ include "common.names.fullname" . }}"
            items:
              - key: "clamd_mod.conf"
                path: "clamd_mod.conf"
        - name: "tmpfs-var-run"
          emptyDir: {}
        - name: "tmpfs-configs"
          emptyDir: {}
        - name: "tmpfs-tmp"
          emptyDir: {}
        {{- if .Values.extraVolumes }}
        {{- include "common.tplvalues.render" (dict "value" .Values.extraVolumes  "context" $) | nindent 8 }}
        {{- end }}
...
