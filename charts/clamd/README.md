<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# clamd

A Helm chart for deploying ClamAV clamd

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository

```console
helm repo add opendesk-clamav https://gitlab.opencode.de/api/v4/projects/1381/packages/helm/stable
helm install my-release --version 4.0.6 opendesk-clamav/clamd
```

### Install via OCI Registry

```console
helm repo add opendesk-clamav oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav
helm install my-release --version 4.0.6 opendesk-clamav/clamd
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity Note: podAffinityPreset, podAntiAffinityPreset, and nodeAffinityPreset will be ignored when it's set |
| commonAnnotations | object | `{}` | Additional custom annotations to add to all deployed objects. |
| commonLabels | object | `{}` | Additional custom labels to add to all deployed objects. |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` | Enable container privileged escalation. |
| containerSecurityContext.capabilities | object | `{"drop":["ALL"]}` | Security capabilities for container. |
| containerSecurityContext.enabled | bool | `true` | Enable security context. |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` | Mounts the container's root filesystem as read-only. |
| containerSecurityContext.runAsGroup | int | `101` | Process group id. |
| containerSecurityContext.runAsNonRoot | bool | `true` | Run container as user. |
| containerSecurityContext.runAsUser | int | `100` | Process user id. |
| containerSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` | Disallow custom Seccomp profile by setting it to RuntimeDefault. |
| extraEnvVars | list | `[]` | Array with extra environment variables to add to containers.  extraEnvVars:   - name: FOO     value: "bar"  |
| extraVolumeMounts | list | `[]` | Optionally specify extra list of additional volumeMounts. |
| extraVolumes | list | `[]` | Optionally specify extra list of additional volumes |
| fullnameOverride | string | `""` | Provide a name to substitute for the full names of resources. |
| global.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| global.persistence.claimName | string | `"clamav-db"` | Name of existing shared Persistent Volume Claim. |
| global.persistence.tmpClaimName | string | `"clamav-tmp"` | Name of existing shared Persistent Volume Claim. |
| global.registry | string | `"docker.io"` | Container registry address. |
| image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| image.registry | string | `""` | Container registry address. This setting has higher precedence than global.registry. |
| image.repository | string | `"clamav/clamav"` | Container repository string. |
| image.tag | string | `"1.1.1-10_base@sha256:aed8d5a3ef58352c862028fae44241215a50eae0b9acb7ba8892b1edc0a6598f"` | Define image tag. |
| imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| lifecycleHooks | object | `{}` | Lifecycle to automate configuration before or after startup |
| livenessProbe.enabled | bool | `true` | Enables kubernetes LivenessProbe. |
| livenessProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| livenessProbe.initialDelaySeconds | int | `15` | Delay after container start until LivenessProbe is executed. |
| livenessProbe.periodSeconds | int | `20` | Time between probe executions. |
| livenessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| livenessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| nameOverride | string | `""` | String to partially override release name. |
| nodeSelector | object | `{}` | Node labels for pod assignment Ref: https://kubernetes.io/docs/user-guide/node-selection/ |
| podAnnotations | object | `{}` | Pod Annotations. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/ |
| podLabels | object | `{}` | Pod Labels. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/ |
| podSecurityContext.enabled | bool | `true` | Enable security context. |
| podSecurityContext.fsGroup | int | `101` | If specified, all processes of the container are also part of the supplementary group |
| podSecurityContext.fsGroupChangePolicy | string | `"Always"` | Change ownership and permission of the volume before being exposed inside a Pod. |
| readinessProbe.enabled | bool | `true` | Enables kubernetes ReadinessProbe. |
| readinessProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| readinessProbe.initialDelaySeconds | int | `15` | Delay after container start until ReadinessProbe is executed. |
| readinessProbe.periodSeconds | int | `20` | Time between probe executions. |
| readinessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| readinessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| replicaCount | int | `1` | Set the amount of replicas of deployment. |
| resources.limits.cpu | int | `4` | The max amount of CPUs to consume. |
| resources.limits.memory | string | `"4Gi"` | The max amount of RAM to consume. |
| resources.requests.cpu | string | `"10m"` | The amount of CPUs which has to be available on the scheduled node. |
| resources.requests.memory | string | `"2Gi"` | The amount of RAM which has to be available on the scheduled node. |
| service.annotations | object | `{}` | Additional custom annotations |
| service.enabled | bool | `true` | Enable kubernetes service creation. |
| service.ports.clamd.containerPort | int | `3310` | Internal port for ClamAV clamd. |
| service.ports.clamd.port | int | `3310` | Accessible port for ClamAV clamd. |
| service.ports.clamd.protocol | string | `"TCP"` | Milter service protocol. |
| service.type | string | `"ClusterIP"` | Choose the kind of Service, one of "ClusterIP", "NodePort" or "LoadBalancer". |
| serviceAccount.annotations | object | `{}` | Additional custom annotations for the ServiceAccount. |
| serviceAccount.automountServiceAccountToken | bool | `false` | Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this serviceAccount do not need to use K8s API. |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for pod. |
| serviceAccount.labels | object | `{}` | Additional custom labels for the ServiceAccount. |
| settings.additionalConfiguration | object | `{}` | Additional custom configuration: |
| settings.alertBrokenExecutables | string | `"yes"` | With this option clamav will try to detect broken executables (both PE and ELF) and alert on them with the Broken.Executable heuristic signature. |
| settings.alertBrokenMedia | string | `"no"` | With this option clamav will try to detect broken media file (JPEG, TIFF, PNG, GIF) and alert on them with a Broken.Media heuristic signature. |
| settings.alertEncrypted | string | `"no"` | Alert on encrypted archives _and_ documents with heuristic signature (encrypted .zip, .7zip, .rar, .pdf). |
| settings.alertEncryptedArchive | string | `"no"` | Alert on encrypted archives with heuristic signature (encrypted .zip, .7zip, .rar). |
| settings.alertEncryptedDoc | string | `"no"` | Alert on encrypted archives with heuristic signature (encrypted .pdf) |
| settings.alertExceedsMax | string | `"no"` | When AlertExceedsMax is set, files exceeding the MaxFileSize, MaxScanSize, or MaxRecursion limit will be flagged with the virus name starting with "Heuristics.Limits.Exceeded". |
| settings.alertOLE2Macros | string | `"no"` | With this option enabled OLE2 files containing VBA macros, which were not detected by signatures will be marked as "Heuristics.OLE2.ContainsMacros". |
| settings.alertPartitionIntersection | string | `"no"` | Alert on raw DMG image files containing partition intersections |
| settings.alertPhishingCloak | string | `"no"` | Alert on cloaked URLs, even if URL isn't in database. This can lead to false positives. |
| settings.alertPhishingSSLMismatch | string | `"no"` | Alert on SSL mismatches in URLs, even if the URL isn't in the database. This can lead to false positives. |
| settings.allowAllMatchScan | string | `"yes"` | Permit use of the ALLMATCHSCAN command. If set to no, clamd will reject any ALLMATCHSCAN command as invalid. |
| settings.bytecode | string | `"yes"` | With this option enabled ClamAV will load bytecode from the database. It is highly recommended you keep this option on, otherwise you'll miss detections for many new viruses. |
| settings.bytecodeSecurity | string | `"TrustSigned"` | Set bytecode security level. Possible values:   None -      No security at all, meant for debugging.               DO NOT USE THIS ON PRODUCTION SYSTEMS.               This value is only available if clamav was built with --enable-debug!   TrustSigned - Trust bytecode loaded from signed .c[lv]d files, insert runtime safety checks for bytecode loaded                 from other sources.   Paranoid -  Don't trust any bytecode, insert runtime checks for all. Recommended: TrustSigned, because bytecode in .cvd files already has these checks. Note that by default only signed bytecode is loaded, currently you can only load unsigned bytecode in --enable-debug mode. |
| settings.bytecodeTimeout | int | `10000` | Set bytecode timeout in milliseconds |
| settings.bytecodeUnsigned | string | `"no"` | Allow loading bytecode from outside digitally signed .c[lv]d files. **Caution**: You should NEVER run bytecode signatures from untrusted sources. Doing so may result in arbitrary code execution. |
| settings.commandReadTimeout | int | `30` | This option specifies the time (in seconds) after which clamd should timeout if a client doesn't provide any initial command after connecting. |
| settings.concurrentDatabaseReload | string | `"yes"` | Enable non-blocking (multi-threaded/concurrent) database reloads. This feature will temporarily load a second scanning engine while scanning continues using the first engine. Once loaded, the new engine takes over. The old engine is removed as soon as all scans using the old engine have completed. This feature requires more RAM, so this option is provided in case users are willing to block scans during reload in exchange for lower RAM requirements. |
| settings.crossFilesystems | string | `"yes"` | Scan files and directories on other filesystems. |
| settings.debug | string | `"no"` | Enable debug messages in libclamav. |
| settings.detectPUA | string | `"no"` | # Detect Possibly Unwanted Applications. |
| settings.disableCache | string | `"no"` | This option allows you to disable the caching feature of the engine. By default, the engine will store an MD5 in a cache of any files that are not flagged as virus or that hit limits checks. Disabling the cache will have a negative performance impact on large scans. |
| settings.disableCertCheck | string | `"no"` | Certain PE files contain an authenticode signature. By default, we check the signature chain in the PE file against a database of trusted and revoked certificates if the file being scanned is marked as a virus. If any certificate in the chain validates against any trusted root, but does not match any revoked certificate, the file is marked as trusted. If the file does match a revoked certificate, the file is marked as virus. The following setting completely turns off authenticode verification. |
| settings.exitOnOOM | string | `"yes"` | Stop daemon when libclamav reports out of memory condition. |
| settings.extendedDetectionInfo | string | `"yes"` | Log additional information about the infected file, such as its size and hash, together with the virus name. |
| settings.fixStaleSocket | string | `"yes"` | Remove stale socket after unclean shutdown. |
| settings.followDirectorySymlinks | string | `"no"` | Follow directory symlinks. |
| settings.followFileSymlinks | string | `"no"` | Follow regular file symlinks. |
| settings.forceToDisk | string | `"yes"` | This option causes memory or nested map scans to dump the content to disk. If you turn on this option, more data is written to disk and is available when the LeaveTemporaryFiles option is enabled. |
| settings.generateMetadataJson | string | `"yes"` | Record metadata about the file being scanned. Scan metadata is useful for file analysis purposes and for debugging scan behavior. The JSON metadata will be printed after the scan is complete if Debug is enabled. A metadata.json file will be written to the scan temp directory if LeaveTemporaryFiles is enabled. |
| settings.heuristicAlerts | string | `"yes"` | In some cases (eg. complex malware, exploits in graphic files, and others), ClamAV uses special algorithms to detect abnormal patterns and behaviors that may be malicious. This option enables alerting on such heuristically detected potential threats. |
| settings.heuristicScanPrecedence | string | `"no"` | Allow heuristic alerts to take precedence. When enabled, if a heuristic scan (such as phishingScan) detects a possible virus/phish it will stop scan immediately. Recommended, saves CPU scan-time. When disabled, virus/phish detected by heuristic scans will be reported only at the end of a scan. If an archive contains both a heuristically detected virus/phish, and a real malware, the real malware will be reported  Keep this disabled if you intend to handle "Heuristics.*" viruses differently from "real" malware. If a non-heuristically-detected virus (signature-based) is found first, the scan is interrupted immediately, regardless of this config option. |
| settings.idleTimeout | int | `30` | Waiting for a new job will timeout after this time (seconds). |
| settings.leaveTemporaryFiles | string | `"no"` | Do not remove temporary files (for debug purposes). |
| settings.localSocket | string | `"/tmp/clamd.sock"` | Path to a local socket file the daemon will listen on. |
| settings.localSocketGroup | string | `""` | Sets the group ownership on the unix socket. |
| settings.localSocketMode | int | `660` | Sets the group ownership on the unix socket. |
| settings.logClean | string | `"no"` | This option allows to tune what is logged when no threat is found in a scanned message. See logInfected for possible values and caveats. |
| settings.logFacility | string | `"LOG_LOCAL6"` | Specify the type of syslog messages. |
| settings.logFile | string | `""` | Path to the log file. |
| settings.logFileMaxSize | string | `"1M"` | Maximum size of the log file. Value of 0 disables the limit. You may use 'M' or 'm' for megabytes (1M = 1m = 1048576 bytes) and 'K' or 'k' for kilobytes (1K = 1k = 1024 bytes). in bytes just don't use modifiers. If LogFileMaxSize is enabled, log rotation (the LogRotate option) will always be enabled. |
| settings.logFileUnlock | string | `"no"` | By default the log file is locked for writing - the lock protects against running clamav multiple times. This option disables log file locking. |
| settings.logInfected | string | `""` | This option allows to tune what is logged when a message is infected. Possible values are Off (the default - nothing is logged), Basic (minimal info logged), Full (verbose info logged)  Note: For this to work properly in sendmail, make sure the msg_id, mail_addr, rcpt_addr and i macroes are available in eom. In other words add a line like: Milter.macros.eom={msg_id}, {mail_addr}, {rcpt_addr}, i to your .cf file. Alternatively use the macro: define(`confMILTER_MACROS_EOM', `{msg_id}, {mail_addr}, {rcpt_addr}, i') Postfix should be working fine with the default settings. |
| settings.logRotate | string | `"no"` | Enable log rotation. Always enabled when logFileMaxSize is enabled. |
| settings.logSyslog | string | `"yes"` | Use system logger (can work together with updateLogFile). |
| settings.logTime | string | `"no"` | Log time with each message. |
| settings.logVerbose | string | `"no"` | Enable verbose logging |
| settings.maxConnectionQueueLength | int | `200` | Maximum length the queue of pending connections may grow to. |
| settings.maxDirectoryRecursion | int | `15` | Maximum depth directories are scanned at. |
| settings.maxEmbeddedPE | string | `"40M"` | Maximum size of a file to check for embedded PE. Files larger than this value will skip the additional analysis step. Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.maxFileSize | string | `"100M"` | Files larger than this limit won't be scanned. Affects the input file itself as well as files contained inside it (when the input file is an archive, a document or some other kind of container). Value of 0 disables the limit. Note: disabling this limit or setting it too high may result in severe damage to the system. Technical design limitations prevent ClamAV from scanning files greater than 2 GB at this time. |
| settings.maxFiles | int | `10000` | Number of files to be scanned within an archive, a document, or any other container file. Value of 0 disables the limit. Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.maxHTMLNoTags | string | `"8M"` | Maximum size of a normalized HTML file to scan. HTML files larger than this value after normalization will not be scanned. Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.maxHTMLNormalize | string | `"40M"` | Maximum size of a HTML file to normalize. HTML files larger than this value will not be normalized or scanned. Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.maxIconsPE | int | `100` | This option sets the maximum number of icons within a PE to be scanned. PE files with more icons than this value will have up to the value number icons scanned. Negative values are not allowed. WARNING: setting this limit too high may result in severe damage or impact performance. |
| settings.maxPartitions | int | `50` | This option sets the maximum number of partitions of a raw disk image to be scanned.    # Raw disk images with more partitions than this value will have up to    # the value number partitions scanned. Negative values are not allowed. |
| settings.maxQueue | int | `100` | Maximum number of queued items (including those being processed by MaxThreads threads). It is recommended to have this value at least twice MaxThreads if possible. |
| settings.maxRecHWP3 | int | `16` | This option sets the maximum recursive calls for HWP3 parsing during scanning. HWP3 files using more than this limit will be terminated and alert the user. Scans will be unable to scan any HWP3 attachments if the recursive limit is reached. Negative values are not allowed. WARNING: setting this limit too high may result in severe damage or impact performance. |
| settings.maxRecursion | int | `17` | Nested archives are scanned recursively, e.g. if a Zip archive contains a RAR file, all files within it will also be scanned. This options specifies how deeply the process should be continued. Note: setting this limit too high may result in severe damage to the system. |
| settings.maxScanSize | string | `"400M"` | This option sets the maximum amount of data to be scanned for each input file. Archives and other containers are recursively extracted and scanned up to this value. Value of 0 disables the limit Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.maxScanTime | int | `120000` | This option sets the maximum amount of time to a scan may take. In this version, this field only affects the scan time of ZIP archives. Value of 0 disables the limit. Note: disabling this limit or setting it too high may result allow scanning of certain files to lock up the scanning process/threads resulting in a Denial of Service. Time is in milliseconds. |
| settings.maxScriptNormalize | string | `"20M"` | Maximum size of a script file to normalize. Script content larger than this value will not be normalized or scanned. Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.maxThreads | int | `10` | Maximum number of threads running at the same time. |
| settings.maxZipTypeRcg | string | `"1M"` | Maximum size of a ZIP file to reanalyze type recognition. ZIP files larger than this value will skip the step to potentially reanalyze as PE. Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.officialDatabaseOnly | string | `"no"` | Only load the official signatures published by the ClamAV project. |
| settings.onAccessCurlTimeout | int | `5000` | Max amount of time (in milliseconds) that the OnAccess client should spend for every connect, send, and recieve attempt when communicating with clamd via curl. |
| settings.onAccessDenyOnError | string | `"no"` | When using prevention, if this option is turned on, any errors that occur during scanning will result in the event attempt being denied. This could potentially lead to unwanted system behaviour with certain configurations, so the client defaults this to off and prefers allowing access events in case of scan or connection error. |
| settings.onAccessDisableDDD | string | `"no"` | Toggles dynamic directory determination. Allows for recursively watching include paths. |
| settings.onAccessExcludeRootUID | string | `"no"` | With this option you can exclude the root UID (0). Processes run under root with be able to access all files without triggering scans or permission denied events. Note that if clamd cannot check the uid of the process that generated an on-access scan event (e.g., because OnAccessPrevention was not enabled, and the process already exited), clamd will perform a scan.  Thus, setting OnAccessExcludeRootUID is not *guaranteed* to prevent every access by the root user from triggering a scan (unless OnAccessPrevention is enabled). |
| settings.onAccessExcludeUID | int | `0` | With this option you can exclude specific UIDs. Processes with these UIDs will be able to access all files without triggering scans or permission denied events. This option can be used multiple times (one per line). Using a value of 0 on any line will disable this option entirely. To exclude the root UID (0) please enable the OnAccessExcludeRootUID option. Also note that if clamd cannot check the uid of the process that generated an on-access scan event (e.g., because OnAccessPrevention was not enabled, and the process already exited), clamd will perform a scan.  Thus, setting OnAccessExcludeUID is not *guaranteed* to prevent every access by the specified uid from triggering a scan (unless OnAccessPrevention is enabled). |
| settings.onAccessExcludeUname | string | `"no"` | This option allows exclusions via user names when using the on-access scanning client. It can be used multiple times. It has the same potential race condition limitations of the OnAccessExcludeUID option. |
| settings.onAccessExtraScanning | string | `"no"` | Toggles extra scanning and notifications when a file or directory is created or moved. Requires the DDD system to kick-off extra scans. |
| settings.onAccessMaxFileSize | string | `"5M"` | Don't scan files larger than OnAccessMaxFileSize Value of 0 disables the limit. |
| settings.onAccessMaxThreads | int | `5` | Max number of scanning threads to allocate to the OnAccess thread pool at startup. These threads are the ones responsible for creating a connection with the daemon and kicking off scanning after an event has been processed. To prevent clamonacc from consuming all clamd's resources keep this lower than clamd's max threads. |
| settings.onAccessPrevention | string | `"no"` | Modifies fanotify blocking behaviour when handling permission events. If off, fanotify will only notify if the file scanned is a virus, and not perform any blocking. |
| settings.onAccessRetryAttempts | int | `0` | Number of times the OnAccess client will retry a failed scan due to connection problems (or other issues). |
| settings.pcreMatchLimit | int | `100000` | This option sets the maximum calls to the PCRE match function during  an instance of regex matching. Instances using more than this limit will be terminated and alert the user but the scan will continue. For more information on match_limit, see the PCRE documentation. Negative values are not allowed. WARNING: setting this limit too high may severely impact performance. |
| settings.pcreMaxFileSize | string | `"100M"` | This option sets the maximum filesize for which PCRE subsigs will be executed. Files exceeding this limit will not have PCRE subsigs executed unless a subsig is encompassed to a smaller buffer. Negative values are not allowed. Setting this value to zero disables the limit. WARNING: setting this limit too high or disabling it may severely impact performance. |
| settings.pcreRecMatchLimit | int | `2000` | This option sets the maximum recursive calls to the PCRE match function during an instance of regex matching. Instances using more than this limit will be terminated and alert the user but the scan will continue. For more information on match_limit_recursion, see the PCRE documentation. Negative values are not allowed and values > PCREMatchLimit are superfluous. WARNING: setting this limit too high may severely impact performance. |
| settings.phishingScanURLs | string | `"yes"` | With this option enabled ClamAV will try to detect phishing attempts by analyzing URLs found in emails using WDB and PDB signature databases. |
| settings.phishingSignatures | string | `"yes"` | With this option enabled ClamAV will try to detect phishing attempts by using HTML.Phishing and Email.Phishing NDB signatures. |
| settings.pidFile | string | `"/tmp/clamd.pid"` | This option allows you to save a process identifier of the listening daemon (main thread). This file will be owned by root, as long as clamd was started by root. It is recommended that the directory where this file is stored is also owned by root to keep other users from tampering with it. |
| settings.preludeAnalyzerName | string | `"ClamAV"` | Set the name of the analyzer used by prelude-admin. |
| settings.preludeEnable | string | `"no"` | Enable Prelude output. |
| settings.readTimeout | int | `120` | Waiting for data from a client socket will timeout after this time (seconds). |
| settings.scanArchive | string | `"yes"` | ClamAV can scan within archives and compressed files. If you turn off this option, the original files will still be scanned, but without unpacking and additional processing. |
| settings.scanELF | string | `"yes"` | Executable and Linking Format is a standard format for UN*X executables. This option allows you to control the scanning of ELF files. If you turn off this option, the original files will still be scanned, but without additional processing. |
| settings.scanHTML | string | `"yes"` | Perform HTML normalisation and decryption of MS Script Encoder code. If you turn off this option, the original files will still be scanned, but without additional processing. |
| settings.scanHWP3 | string | `"yes"` | This option enables scanning of HWP3 files. If you turn off this option, the original files will still be scanned, but without additional processing. |
| settings.scanMail | string | `"yes"` | Enable internal e-mail scanner. If you turn off this option, the original files will still be scanned, but without parsing individual messages/attachments. |
| settings.scanOLE2 | string | `"yes"` | This option enables scanning of OLE2 files, such as Microsoft Office documents and .msi files. If you turn off this option, the original files will still be scanned, but without additional processing. |
| settings.scanPDF | string | `"yes"` | This option enables scanning within PDF files. If you turn off this option, the original files will still be scanned, but without decoding and additional processing. |
| settings.scanPE | string | `"yes"` | PE stands for Portable Executable - it's an executable file format used in all 32 and 64-bit versions of Windows operating systems. This option allows ClamAV to perform a deeper analysis of executable files and it's also required for decompression of popular executable packers such as UPX, FSG, and Petite. If you turn off this option, the original files will still be scanned, but without additional processing. |
| settings.scanPartialMessages | string | `"no"` | Scan RFC1341 messages split over many emails. You will need to periodically clean up $TemporaryDirectory/clamav-partial directory. WARNING: This option may open your system to a DoS attack. |
| settings.scanSWF | string | `"yes"` | This option enables scanning within SWF files. If you turn off this option, the original files will still be scanned, but without decoding and additional processing. |
| settings.scanXMLDOCS | string | `"yes"` | This option enables scanning xml-based document files supported by libclamav. If you turn off this option, the original files will still be scanned, but without additional processing. |
| settings.selfCheck | int | `120` |  |
| settings.sendBufTimeout | int | `500` | This option specifies how long to wait (in milliseconds) if the send buffer is full. Keep this value low to prevent clamd hanging. |
| settings.streamMaxLength | string | `"100M"` | Close the connection when the data size limit is exceeded. The value should match your MTA's limit for a maximum attachment size. |
| settings.streamMaxPort | int | `2048` | Limit port range. |
| settings.streamMinPort | int | `1024` | Limit port range. |
| settings.structuredCCOnly | string | `"no"` | With this option enabled the DLP module will search for valid Credit Card numbers only. Debit and Private Label cards will not be searched. |
| settings.structuredDataDetection | string | `"no"` | Enable the DLP module |
| settings.structuredMinCreditCardCount | int | `3` | This option sets the lowest number of Credit Card numbers found in a file to generate a detect. |
| settings.structuredMinSSNCount | int | `3` | This option sets the lowest number of Social Security Numbers found in a file to generate a detect. |
| settings.structuredSSNFormatNormal | string | `"yes"` | With this option enabled the DLP module will search for valid SSNs formatted as xxx-yy-zzzz |
| settings.structuredSSNFormatStripped | string | `"no"` | With this option enabled the DLP module will search for valid SSNs formatted as xxxyyzzzz |
| settings.temporaryDirectory | string | `"/var/tmp"` | Optional path to the global temporary directory. |
| settings.user | string | `"clamav"` | Run as another user (clamd must be started by root for this option to work) |
| settings.virusEvent | string | `"no"` | Execute a command when virus is found. In the command string %v will be replaced with the virus name and %f will be replaced with the file name. Additionally, two environment variables will be defined: $CLAM_VIRUSEVENT_FILENAME and $CLAM_VIRUSEVENT_VIRUSNAME. |
| terminationGracePeriodSeconds | string | `""` | In seconds, time the given to the pod needs to terminate gracefully. Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods |
| tolerations | list | `[]` | Tolerations for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/ |
| topologySpreadConstraints | list | `[]` | Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/  topologySpreadConstraints:   - maxSkew: 1     topologyKey: failure-domain.beta.kubernetes.io/zone     whenUnsatisfiable: DoNotSchedule |
| updateStrategy.type | string | `"RollingUpdate"` | Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pods is destroyed first. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
