{{/*
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
---
apiVersion: {{ include "common.capabilities.deployment.apiVersion" . }}
kind: "Deployment"
metadata:
  name: {{ include "common.names.fullname" . }}
  namespace: {{ include "common.names.namespace" . | quote }}
  labels: {{- include "common.labels.standard" . | nindent 4 }}
    app.kubernetes.io/component: "freshclam"
    {{- if .Values.commonLabels }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.commonLabels "context" $ ) | nindent 4 }}
    {{- end }}
  {{- if .Values.commonAnnotations }}
  annotations: {{- include "common.tplvalues.render" ( dict "value" .Values.commonAnnotations "context" $ ) | nindent 4 }}
  {{- end }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      {{- include "common.labels.matchLabels" . | nindent 6 }}
  strategy: {{ include "common.tplvalues.render" (dict "value" .Values.updateStrategy "context" $) | nindent 4 }}
  template:
    metadata:
      annotations:
        checksum/configmap: {{ include (print .Template.BasePath "/configmap.yaml") . | sha256sum }}
        {{- if .Values.podAnnotations }}
        {{- include "common.tplvalues.render" (dict "value" .Values.podAnnotations "context" $) | nindent 8 }}
        {{- end }}
      labels:
        {{- include "common.labels.standard" . | nindent 8 }}
    spec:
      {{- if or .Values.imagePullSecrets .Values.global.imagePullSecrets }}
      imagePullSecrets:
        {{- range .Values.global.imagePullSecrets }}
        - name: "{{ . }}"
        {{- end }}
        {{- range .Values.imagePullSecrets }}
        - name: "{{ . }}"
        {{- end }}
      {{- end }}
      {{- if .Values.affinity }}
      affinity: {{- include "common.tplvalues.render" (dict "value" .Values.affinity "context" $) | nindent 8 }}
      {{- end }}
      {{- if .Values.tolerations }}
      tolerations: {{- include "common.tplvalues.render" (dict "value" .Values.tolerations "context" $) | nindent 8 }}
      {{- end }}
      {{- if .Values.topologySpreadConstraints }}
        topologySpreadConstraints: {{- include "common.tplvalues.render" (dict "value" .Values.topologySpreadConstraints "context" $) | nindent 8 }}
      {{- end }}
      {{- if .Values.nodeSelector }}
      nodeSelector: {{- include "common.tplvalues.render" (dict "value" .Values.nodeSelector "context" $) | nindent 8 }}
      {{- end }}
      {{- if .Values.podSecurityContext.enabled }}
      securityContext: {{- omit .Values.podSecurityContext "enabled" | toYaml | nindent 8 }}
      {{- end }}
      {{- if .Values.serviceAccount.create }}
      serviceAccountName: {{ include "common.names.fullname" . }}
      {{- end }}
      {{- if .Values.terminationGracePeriodSeconds }}
      terminationGracePeriodSeconds: {{ .Values.terminationGracePeriodSeconds }}
      {{- end }}
      initContainers:
        - name: "dependencies"
          {{- if .Values.containerSecurityContext.enabled }}
          securityContext: {{- omit .Values.containerSecurityContext "enabled" | toYaml | nindent 12 }}
          {{- end }}
          image: "{{ coalesce .Values.image.registry .Values.global.registry }}/{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.imagePullPolicy }}
          command:
            - "/bin/sh"
            - "-c"
            - |
              until [ $(stat -c "%U" /var/lib/clamav) = "clamav" ]
              do echo 'Directory owner of /var/lib/clamav is not clamav'
              sleep 5
              done
          {{- if .Values.resources }}
          resources: {{- include "common.tplvalues.render" (dict "value" .Values.resources "context" $) | nindent 12 }}
          {{- end }}
          volumeMounts:
            - name: "clamav-database"
              mountPath: "/var/lib/clamav/"
      containers:
        - name: "freshclam"
          {{- if .Values.containerSecurityContext.enabled }}
          securityContext: {{- omit .Values.containerSecurityContext "enabled" | toYaml | nindent 12 }}
          {{- end }}
          image: "{{ coalesce .Values.image.registry .Values.global.registry }}/{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.imagePullPolicy }}
          env:
            - name: "CLAMAV_NO_CLAMD"
              value: "true"
            - name: "CLAMAV_NO_FRESHCLAMD"
              value: "false"
            - name: "CLAMAV_NO_MILTERD"
              value: "true"
          {{- with .Values.extraEnvVars }}
            {{- . | toYaml | nindent 12 }}
          {{- end }}
          livenessProbe:
            exec:
              command:
                - sh
                - -c
                - pgrep freshclam &> /dev/null
            initialDelaySeconds: {{ .Values.livenessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.livenessProbe.periodSeconds }}
            timeoutSeconds: {{ .Values.livenessProbe.timeoutSeconds }}
            failureThreshold: {{ .Values.livenessProbe.failureThreshold }}
            successThreshold: {{ .Values.livenessProbe.successThreshold }}
          readinessProbe:
            exec:
              command:
                - sh
                - -c
                - pgrep freshclam &> /dev/null
            initialDelaySeconds: {{ .Values.readinessProbe.initialDelaySeconds }}
            periodSeconds: {{ .Values.readinessProbe.initialDelaySeconds }}
            timeoutSeconds: {{ .Values.readinessProbe.timeoutSeconds }}
            failureThreshold: {{ .Values.readinessProbe.failureThreshold }}
            successThreshold: {{ .Values.readinessProbe.successThreshold }}
          {{- if .Values.resources }}
          resources: {{- include "common.tplvalues.render" (dict "value" .Values.resources "context" $) | nindent 12 }}
          {{- end }}
          {{- if .Values.lifecycleHooks }}
          lifecycle: {{- include "common.tplvalues.render" (dict "value" .Values.lifecycleHooks "context" $) | nindent 12 }}
          {{- end }}
          volumeMounts:
            - name: "clamav-database"
              mountPath: "/var/lib/clamav/"
            - name: "config"
              mountPath: "/etc/clamav"
            - name: "tmpfs-run-clamav"
              mountPath: "/run/clamav"
            - name: "tmpfs-run-lock"
              mountPath: "/run/lock"
            - name: "tmpfs-var-lock"
              mountPath: "/var/lock"
            {{- if .Values.settings.updateLogFile }}
            - name: "tmpfs-var-log-clamav-log"
              mountPath: "/var/log/clamav/"
            {{- end }}
            {{- if .Values.extraVolumeMounts }}
            {{- include "common.tplvalues.render" (dict "value" .Values.extraVolumeMounts "context" $) | nindent 12 }}
            {{- end }}
      volumes:
        - name: "config"
          configMap:
            name: "{{ include "common.names.fullname" . }}"
        - name: "clamav-database"
          persistentVolumeClaim:
            claimName: "{{ .Values.global.persistence.claimName }}"
        - name: "tmpfs-run-clamav"
          emptyDir: {}
        - name: "tmpfs-run-lock"
          emptyDir: {}
        - name: "tmpfs-var-lock"
          emptyDir: {}
        {{- if .Values.settings.updateLogFile }}
        - name: "tmpfs-var-log-clamav-log"
          emptyDir: {}
        {{- end }}
        {{- if .Values.extraVolumes }}
        {{- include "common.tplvalues.render" (dict "value" .Values.extraVolumes  "context" $) | nindent 8 }}
        {{- end }}
...
