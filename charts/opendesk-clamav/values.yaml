# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
#
# SPDX-License-Identifier: Apache-2.0
---
# The global properties are used to configure multiple charts at once.
global:

  # -- Container registry address.
  registry: "docker.io"

  # -- Credentials to fetch images from private registry
  # Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  #
  # imagePullSecrets:
  #   - "docker-registry"
  #
  imagePullSecrets: []

  # This claim is used to share the database with other ClamAV services.
  persistence:
    # -- Name of existing shared Persistent Volume Claim.
    claimName: "clamav-db"
    # -- Name of existing shared Persistent Volume Claim.
    tmpClaimName: "clamav-tmp"


# Security Context.
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
containerSecurityContext:
  # -- Enable container privileged escalation.
  allowPrivilegeEscalation: false

  # -- Enable security context.
  enabled: true

  # -- Mounts the container's root filesystem as read-only.
  readOnlyRootFilesystem: true

# Container image section.
image:
  # -- Container registry address. This setting has higher precedence than global.registry.
  registry: ""

  # -- Container repository string.
  repository: "clamav/clamav"

  # -- Define an ImagePullPolicy.
  #
  # Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy
  #
  # "IfNotPresent" => The image is pulled only if it is not already present locally.
  # "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to
  #             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached
  #             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved
  #             digest, and uses that image to launch the container.
  # "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the
  #            kubelet attempts to start the container; otherwise, startup fails
  #
  imagePullPolicy: "IfNotPresent"

  # -- Define image tag.
  tag: "1.1.1-10_base@sha256:aed8d5a3ef58352c862028fae44241215a50eae0b9acb7ba8892b1edc0a6598f"


# Overwrite milter connection to clamd
# @ignore
milter:
  settings:
    #clamdHost: "{{ .Release.Name }}-clamd"
    clamdHost: "clamav-clamd"

# Whether to allocate persistent volume disk for the data directory.
# In case of node failure, the node data directory will still persist.
#
# Ref.: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
persistence:
  # -- Enable data persistence (true) or use temporary storage (false).
  enabled: true

  # -- The volume access modes, some of "ReadWriteOnce", "ReadOnlyMany", "ReadWriteMany", "ReadWriteOncePod".
  # "ReadWriteOnce" => The volume can be mounted as read-write by a single node. ReadWriteOnce access mode still can
  #                    allow multiple pods to access the volume when the pods are running on the same node.
  # "ReadOnlyMany" => The volume can be mounted as read-only by many nodes.
  # "ReadWriteMany" => The volume can be mounted as read-write by many nodes.
  # "ReadWriteOncePod" => The volume can be mounted as read-write by a single Pod. Use ReadWriteOncePod access mode if
  #                       you want to ensure that only one pod across whole cluster can read that PVC or write to it.
  #
  accessModes:
    - "ReadWriteMany"

  # -- Annotations for the PVC.
  annotations: {}

  # -- Custom PVC data source.
  dataSource: {}

  # -- Labels for the PVC.
  labels: {}

  # -- The volume size with unit.
  size: "1Gi"

  # -- The (storage) class of PV.
  storageClass: ""

  # -- Selector to match an existing Persistent Volume (this value is evaluated as a template)
  #
  # selector:
  #   matchLabels:
  #     app: my-app
  #
  selector: {}

# Pod Security Context.
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
podSecurityContext:
  # -- Enable security context.
  enabled: false

# Configure resource requests and limits.
#
# http://kubernetes.io/docs/user-guide/compute-resources/
#
resources:
  limits:
    # -- The max amount of CPUs to consume.
    cpu: 1
    # -- The max amount of RAM to consume.
    memory: "128Mi"
  requests:
    # -- The amount of CPUs which has to be available on the scheduled node.
    cpu: "10m"
    # -- The amount of RAM which has to be available on the scheduled node.
    memory: "16Mi"

...
