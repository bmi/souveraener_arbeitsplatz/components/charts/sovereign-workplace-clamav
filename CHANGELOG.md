## [4.0.6](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav/compare/v4.0.5...v4.0.6) (2024-08-28)


### Bug Fixes

* **clamav-simple:** Add config as volumeMount ([496406a](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav/commit/496406a56e5f130bffc4ee961fd41caf2a1a2365))

## [4.0.5](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav/compare/v4.0.4...v4.0.5) (2024-03-12)


### Bug Fixes

* **clamav-simple:** Fix updateStrategy in StatefulSet ([da3e6b3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav/commit/da3e6b3768dc3a0dba465f22c5465df13e3bdbae))

## [4.0.4](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav/compare/v4.0.3...v4.0.4) (2024-03-12)


### Bug Fixes

* **clamav-simple:** Fix serviceName in StatefulSet ([73e876e](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav/commit/73e876e16092372413454c28f53d23995078cddf))

## [4.0.3](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav/compare/v4.0.2...v4.0.3) (2024-03-12)


### Bug Fixes

* **clamav-simple:** Add serviceName to StatefulSet ([5a43eeb](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav/commit/5a43eeba218887c9d0604f23358a70bebd43884b))

## [4.0.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav/compare/v4.0.1...v4.0.2) (2023-12-21)


### Bug Fixes

* Add GPG key, update README.md ([17369f8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav/commit/17369f8520aeb0adf91cc0adb3271790cb22d4f7))

## [4.0.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav/compare/v4.0.0...v4.0.1) (2023-12-20)


### Bug Fixes

* **ci:** Move repo to Open CoDE ([4c2a070](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav/commit/4c2a0703118525c467869380f7d8ce3e26d3c8e6))

# [4.0.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/compare/v3.0.0...v4.0.0) (2023-08-31)


### Bug Fixes

* Rename sovereign-workplace to opendesk ([a01fc4e](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/commit/a01fc4e9aceb4813d5016a3910e5a6074e6db541))


### BREAKING CHANGES

* Rename sovereign-workplace-clamav to opendesk-clamav
* Rename sovereign-workplace-icap to icap

# [3.0.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/compare/v2.1.0...v3.0.0) (2023-08-30)


### Bug Fixes

* **freshclam:** Improve default security settings ([99ff016](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/commit/99ff016c7a1ccb80067433a8aacd565729ed89b3))
* **milter:** Improve default security settings ([97b3557](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/commit/97b3557b5a191414d7341c5def73ac7ca63d8d6a))


### Features

* **clamav-simple:** Use alpine based ICAP image ([4bb6292](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/commit/4bb6292ffacd68550536ac80b2d67f877b1b4d61))
* **clamd:** Improve default security settings, add volumes for icap shares ([0e10a65](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/commit/0e10a65b00183ba2d93afafc5a953eaee2724822))
* **sovereign-workplace-clamav:** Add temClaimName for file share between ICAP and clamd ([a353ed5](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/commit/a353ed5f2da0b5fbf5c48e2b6d4761045e8f7902))
* **sovereign-workplace-icap:** Use alpine based image to connect to clamd and add settings ([3a3c184](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/commit/3a3c18437f9654283d2b1ffdee5988266124da05))


### BREAKING CHANGES

* **milter:** Replace settings.clamdSocket by settings.clamdHost and settings.clamdPort

# [2.1.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/compare/v2.0.2...v2.1.0) (2023-07-27)


### Features

* **clamav-simple:** Add clamav-simple chart ([cf5e003](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/commit/cf5e0036808fd5559586499da9453b6972b1f180))

## [2.0.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/compare/v2.0.1...v2.0.2) (2023-07-17)


### Bug Fixes

* **sovereign-workplace-clamav:** Add README template ([43176f8](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/commit/43176f8e9eb0d5723d5c93181d01502b9c0f3ad0))

## [2.0.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/compare/v2.0.0...v2.0.1) (2023-07-17)


### Bug Fixes

* **charts:** Add resources and securityContext to job ([bc1d731](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/commit/bc1d731b8d01a35e34237cecfe29499042d43890))
* **sovereign-workplace-clamav:** Update versions in dependencies ([61c58ad](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/commit/61c58ad4b43ac3570eb2c6e4461dde09359534a5))

# [2.0.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/compare/v1.2.2...v2.0.0) (2023-07-17)


### Features

* **charts:** Split into multiple charts ([544dc9b](https://gitlab.souvap-univention.de/souvap/tooling/charts/clamav/commit/544dc9b363030073f1e22958dad80ba5512b03a6))


### BREAKING CHANGES

* **charts:** Refactor into multiple independent charts

<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"

SPDX-License-Identifier: Apache-2.0
-->
