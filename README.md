<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"

SPDX-License-Identifier: Apache-2.0
-->
# openDesk ClamAV Helm Charts

This repository contains a Helm chart for deploying ClamAV as separate scalable microservices using a ReadWriteMany
volume [opendesk-clamav](charts/opendesk-clamav) consisting of [freshclam](charts/freshclam),
[clamd](charts/clamd), [milter](charts/milter) and [icap](charts/icap) or as a
single scalable StatefulSet using ReadWriteOnce volumes [clamav-simple](charts/clamav-simple).


## Prerequisites

Before you begin, ensure you have met the following requirements:

- Kubernetes 1.21+
- Helm 3.0.0+
- PV provisioner support in the underlying infrastructure


## Documentation

The documentation is placed in the README of each helm chart:

- [clamav-simple](charts/clamav-simple)
- [clamd](charts/clamd)
- [freshclam](charts/freshclam)
- [icap](charts/icap)
- [milter](charts/milter)
- [openDesk-clamav](charts/opendesk-clamav)

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
